require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Home | Stripe Test App"
  end
  test "should get payment" do
    get :payment
    assert_response :success
    assert_select "title", "Payment | Stripe Test App"
  end
end
